set term png
set output "distribution.png"
set xlabel "Server ID"
set ylabel "Number of keys on a node"

set boxwidth 0.4
set style fill solid 1.00
set nokey
set title "Object distribution accross nodes (number of objects = 2000)"

# Replace small stripes on the Y-axis with a horizontal gridlines
set tic scale 0
set grid ytics

# Remove border around chart
unset border

plot "distribution.dat" using 2:xticlabels(1) with boxes lt rgb "#406090"
