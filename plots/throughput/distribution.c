#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define NO_LINES_MAX	1000
#define MAX_VALUE_SIZE	1000

#define PUT			"put"
#define GET			"get"
#define MAX_LENGTH	2500
#define MIN_LENGTH	5

#define MIN_ASCII	33
#define MAX_ASCII	126

unsigned int rand_interval(unsigned int min, unsigned int max)
{
	int r;
	const unsigned int range = 1 + max - min;
	const unsigned int buckets = RAND_MAX / range;
	const unsigned int limit = buckets * range;

	do {
		r = rand();
	} while (r >= limit);

	return min + (r / buckets);
}


int main()
{
	int i, j, k, key_length, value_length;
	char key[MAX_LENGTH], value[MAX_LENGTH];
	char out_put_filename[30], out_get_filename[30];
	FILE *fd_put, *fd_get;


	for (i = 1000; i >= 200; i-= 50) {
		sprintf(out_put_filename, "throughput_put_%d.txt", i);
		sprintf(out_get_filename, "throughput_get_%d.txt", i);

		fd_put = fopen(out_put_filename, "w");
		if (NULL == fd_put) {
			perror("Error ");
			return -1;
		}

		fd_get = fopen(out_get_filename, "w");
		if (NULL == fd_get) {
			perror("Error ");
			return -1;
		}

		srand(time(NULL));

		for (j = 0; j < i; j++) {
			key_length = rand_interval(MIN_LENGTH, 30);
			value_length = 500000/i;

			key[0] = '\0';
			for (k = 0; k < key_length; k++) {
				key[k] = rand_interval(MIN_ASCII, MAX_ASCII);
			}
			key[key_length] = '\0';

			value[0] = '\0';
			for (k = 0; k < value_length; k++) {
				value[k] = rand_interval(MIN_ASCII, MAX_ASCII);
			}
			value[value_length] = '\0';

			fprintf(fd_put,"%s %s %s\n", PUT, key, value);
			fprintf(fd_get,"%s %s\n", GET, key);
		}

		fclose(fd_get);
		fclose(fd_put);
	}
}
