set term png
set output "throughput.png"
set xlabel "Number of operations"
set ylabel "Time to perform all the operations"

set boxwidth 0.4
set style fill solid 1.00
set title "Read/Write throughput for 500KB of data"

# Remove border around chart
unset border
set grid

plot "throughput.dat" using 1:2 with linespoints title "GET", \
"throughput.dat" using 1:3 with linespoints title "PUT"
