#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define OUT_FILE_GET	"availability_get.txt"
#define OUT_FILE_PUT	"availability_put.txt"
#define NO_LINES	100
#define PUT			"put"
#define GET			"get"
#define MAX_LENGTH	20
#define MIN_LENGTH	5

#define MIN_ASCII	33
#define MAX_ASCII	126

unsigned int rand_interval(unsigned int min, unsigned int max)
{
	int r;
	const unsigned int range = 1 + max - min;
	const unsigned int buckets = RAND_MAX / range;
	const unsigned int limit = buckets * range;

	do {
		r = rand();
	} while (r >= limit);

	return min + (r / buckets);
}


int main()
{
	int i, j, key_length, value_length;
	char key[MAX_LENGTH], value[MAX_LENGTH];
	FILE *fd_get, *fd_put;

	fd_get = fopen(OUT_FILE_GET, "w");
	if (NULL == fd_get) {
		perror("Error ");
		return -1;
	}

	fd_put = fopen(OUT_FILE_PUT, "w");
	if (NULL == fd_put) {
		perror("Error ");
		return -1;
	}

	srand(time(NULL));
	for (i = 0; i < NO_LINES; i++) {
		key_length = rand_interval(MIN_LENGTH, MAX_LENGTH);
		value_length = rand_interval(MIN_LENGTH, MAX_LENGTH);

		key[0] = '\0';
		for (j = 0; j < key_length; j++) {
			key[j] = rand_interval(MIN_ASCII, MAX_ASCII);
		}
		key[key_length] = '\0';

		value[0] = '\0';
		for (j = 0; j < value_length; j++) {
			value[j] = rand_interval(MIN_ASCII, MAX_ASCII);
		}
		value[value_length] = '\0';

		fprintf(fd_put,"%s %s %s\n", PUT, key, value);
		fprintf(fd_get,"%s %s\n", GET, key);
	}

	fclose(fd_get);
	fclose(fd_put);
}
