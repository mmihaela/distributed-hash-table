set term png
set output "availability.png"
set xlabel "Number of failed servers"
set ylabel "Percent of successful operations"

set boxwidth 0.4
set style fill solid 1.00
set nokey
set title "Object availability (no objects = 100)"

# Remove border around chart
unset border
set grid

plot "availability.dat" using 1:2 with linespoints lt rgb "#406090"
