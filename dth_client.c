#include <stdlib.h>
#include <string.h>

#include "utils.h"
#include "dth_ops.h"

int main(int argc, char *argv[])
{
	int ret = 0;
	FILE *fd;
	op_t op_pair;
	int get_successful = 0;

	if (2 != argc) {
		printf("Wrong number of parameters!\n");
		printf("How to run: ./dth_client <command_file>\n");
		return -1;
	}

	fd = fopen(argv[1], "r");
	if (NULL == fd) {
		perror("Error opening input file");
		return -1;
	}

	init_hash_ips();

	/* read operation line by line */
	while (1) {
		memset(&op_pair, 0, sizeof(op_t));
		ret = read_operation(fd, &op_pair);
		if (ret == -1)
			break;

		switch (ret) {
			case PUT_TYPE:
				put(op_pair);
				break;
			case GET_TYPE:
				ret = get(op_pair);
				get_successful = (ret == 0)? get_successful + 1: get_successful;
				break;
			default:
				printf("Unknown operation!\n");
		}
	}

	fclose(fd);
	printf("Number of successful gets %d\n", get_successful);

	return 0;
}

