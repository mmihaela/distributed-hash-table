#ifndef UTILS_H
#define UTILS_H

#include <openssl/md5.h>
#include <openssl/sha.h>
#include <stdio.h>

#define INPUT_FILE	"commands.dat"

#define GET			"get"
#define PUT			"put"
#define GET_TYPE	(0)
#define PUT_TYPE	(1)

#define OP_SIZE		4

#define MAX_HASH_LENGTH		(20)

#define MAX_WORD_SIZE		(50)
#define NO_IPS				(10)

#define MD5_TYPE			(0)
#define SHA_TYPE			(1)

typedef struct {
	char key[MAX_WORD_SIZE];
	char value[MAX_WORD_SIZE];
} op_t;


typedef struct {
	char ip[16];
	unsigned char hash[MAX_HASH_LENGTH];
	unsigned int hash_size;
} server_info_t;

void print_hash(unsigned char *digest, int length);
int read_operation(FILE *fd, op_t *op_pair);
void init_hash_ips();
int compare_hash(unsigned char *hash1, unsigned char *hash2, int size);

#endif
