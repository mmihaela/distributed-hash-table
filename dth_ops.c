#include <libmemcached/memcached.h>
#include <openssl/md5.h>
#include <openssl/sha.h>
#include <sys/timeb.h>
#include "dth_ops.h"

extern server_info_t servers_md5[NO_IPS];
extern server_info_t servers_sha[NO_IPS];

static int lookup_server(unsigned char *hash, int length)
{
	int left, right, middle, cmp_ret;
	server_info_t target_hash[NO_IPS];
	
	switch (length) {
		case MD5_DIGEST_LENGTH:
			memcpy(target_hash, servers_md5, NO_IPS * sizeof(server_info_t));
			break;
		case SHA_DIGEST_LENGTH:
			memcpy(target_hash, servers_sha, NO_IPS * sizeof(server_info_t));
			break;
	}

	/* binary search for the server */
	left = 0;
	right = NO_IPS - 1;

	/* if the hash is the last value */
	cmp_ret = compare_hash(hash, target_hash[right].hash, length);
	if (cmp_ret == 1)
		return right;

	while (right - left != 1) {
		middle = (left + right) / 2;
		cmp_ret = compare_hash(hash, target_hash[middle].hash, length);
		if (cmp_ret == -1)
			right = middle;
		else if (cmp_ret == 1)
			left = middle;
		else if (cmp_ret == 0) {
			return middle;
		}
	}

	return left;
}

/**
 * Go through the list of servers and attempt to connect to one of them.
 * If the server that was initially chosen can't be contacted, continue looking for a
 * server available server counter-clockwise.
 */
static int connect_to_memcached_server(memcached_st **memc, int server_idx, int type)
{
	int iter, counter;
	memcached_return_t error;

	counter = 0;
	iter = server_idx;
	do {
		switch(type) {
			case MD5_TYPE:
				error = memcached_server_add(*memc, servers_md5[iter].ip, 0);
				break;
			case SHA_TYPE:
				error = memcached_server_add(*memc, servers_sha[iter].ip, 0);
				break;
		}

		if (MEMCACHED_SUCCESS != error) {
			fprintf(stderr, "memcached_server_add: %s\n", memcached_strerror(*memc, error));
			/* look through the servers until it finds one available to
			 * write the value */
			iter--;
			counter++;
			/* continue from the last server */
			if (iter < 0)
				iter = NO_IPS - 1;
			else if (counter == (NO_IPS / 2))
				return -1;
		} else
			break;
	} while (MEMCACHED_SUCCESS != error);

	return 0;
}

/**
 * Empty server list to be able to write on a single server at a time.
 */
static void free_server_list(memcached_st **memc)
{
	memcached_free(*memc);

	/* recreate context */
	*memc = memcached_create(NULL);

	if (NULL == *memc) {
		printf("Error: unable to create memcached context\n");
		exit(-1);
	}
}


static char *get_value(memcached_st **memc, unsigned char *hash_digest, int hash_type, char *key)
{
	char *value;
	uint32_t flags = 0;
	size_t value_length;
	server_info_t *servers;
	int target_server, hash_length, counter, ret;
	memcached_return_t error;
	struct timeval start_tb, stop_tb;

	switch (hash_type) {
		case MD5_TYPE:
			hash_length = MD5_DIGEST_LENGTH;
			servers = (server_info_t *)servers_md5;
			break;
		case SHA_TYPE:
			hash_length = SHA_DIGEST_LENGTH;
			servers = (server_info_t *)servers_sha;
			break;
	}

	/* look-up the server hosting the value for the hashed key */
	target_server = lookup_server(hash_digest, hash_length);

	/* read value on the server found */
	for (counter = 0; counter < NO_IPS/2; counter++) {
		value = NULL;
		error = memcached_server_add(*memc, servers[target_server].ip, 0);
		if (MEMCACHED_SUCCESS != error) {
			fprintf(stderr, "memcached_server_add: %s\n", memcached_strerror(*memc, error));
		} else {
			ret = gettimeofday(&start_tb, NULL);
			if (ret < 0) {
				fprintf(stderr, "Unable to get time\n");
				break;
			}

			value = memcached_get(*memc, key, strlen(key), &value_length, &flags, &error);
			ret = gettimeofday(&stop_tb, NULL);
			if (ret < 0) {
				fprintf(stderr, "Unable to get time\n");
				break;
			}

			printf("[GET_TIME] %ld\n", (stop_tb.tv_sec - start_tb.tv_sec) * 1000000 + (stop_tb.tv_usec - start_tb.tv_usec));

			if (MEMCACHED_SUCCESS == error) {
				break;
			}
			fprintf(stderr, "memcached_get: %s\n", memcached_strerror(*memc, error));
		}

		free_server_list(memc);
		target_server--;
		if (target_server < 0)
			target_server = NO_IPS - 1;
	}

	return value;
}


int get(op_t op)
{
	int ret;
	char *value;
	memcached_st *memc;
	memcached_return_t error;
	unsigned char md5_digest[MD5_DIGEST_LENGTH];
	unsigned char sha_digest[SHA_DIGEST_LENGTH];

	/* get hash of value */
	MD5((unsigned char *)op.key, strlen(op.key), md5_digest);
	SHA((unsigned char *)op.key, strlen(op.key), sha_digest);

	memc = memcached_create(NULL);
	if (NULL == memc) {
		perror("Error :");
		return -1;
	}

	value = get_value(&memc, md5_digest, MD5_TYPE, op.key);
	if (NULL == value) {
		goto sha_lookup;
	}

	/* successfuly got the value for the key */
	free(value);
	ret = 0;
	goto out;

sha_lookup:
	value = get_value(&memc, sha_digest, SHA_TYPE, op.key);
	if (NULL == value) {
		ret = -1;
		goto out;
	}

	/* successfuly got the value for the key */
	free(value);
	ret = 0;

out:
	memcached_free(memc);
	return ret;
}

int put(op_t op)
{
	memcached_st *memc;
	memcached_return_t error;
	struct timeval start_tb, stop_tb;
	unsigned char md5_digest[MD5_DIGEST_LENGTH];
	unsigned char sha_digest[SHA_DIGEST_LENGTH];
	int target_server_md5, target_server_sha, ret;

	/* get hash of value */
	MD5((unsigned char *)op.key, strlen(op.key), md5_digest);
	SHA((unsigned char *)op.key, strlen(op.key), sha_digest);

	/* lookup the server on which the value should be put */
	target_server_md5 = lookup_server(md5_digest, MD5_DIGEST_LENGTH);

	target_server_sha = lookup_server(sha_digest, SHA_DIGEST_LENGTH);

	/* use memcached to write the value on the chosen servers */
	memc = memcached_create(NULL);
	if (NULL == memc) {
		perror("Error :");
		return -1;
	}

	/* write on MD5 looked-up server */
	ret = connect_to_memcached_server(&memc, target_server_md5, MD5_TYPE);
	if (-1 == ret) {
		printf("Error: no servers were found!");
		goto out;
	}

	ret = gettimeofday(&start_tb, NULL);
	if (ret < 0) {
		fprintf(stderr, "Unable to get time\n");
		return;
	}
	/* put the value */
	error = memcached_set(memc, op.key, strlen(op.key), op.value, strlen(op.value), 0, 0);
	ret = gettimeofday(&stop_tb, NULL);
	if (ret < 0) {
		fprintf(stderr, "Unable to get time\n");
		return;
	}
	printf("[PUT_TIME] %ld\n", (stop_tb.tv_sec - start_tb.tv_sec) * 1000000 + (stop_tb.tv_usec - start_tb.tv_usec));

	if (MEMCACHED_SUCCESS != error) {
		fprintf(stderr, "memcached_set (md5): %s\n", memcached_strerror(memc, error));
		ret = -1;
		goto out;
	}

	/* write on SHA looked-up server */
	ret = connect_to_memcached_server(&memc, target_server_sha, SHA_TYPE);
	if (-1 == ret) {
		printf("Error: no servers were found\n");
		goto out;
	}

	ret = gettimeofday(&start_tb, NULL);
	if (ret < 0) {
		fprintf(stderr, "Unable to get time\n");
		return;
	}
	/* put the value */
	error = memcached_set(memc, op.key, strlen(op.key), op.value, strlen(op.value), 0, 0);

	ret = gettimeofday(&stop_tb, NULL);
	if (ret < 0) {
		fprintf(stderr, "Unable to get time\n");
		return;
	}

	printf("[PUT_TIME] %ld\n", (stop_tb.tv_sec - start_tb.tv_sec) * 1000000 + (stop_tb.tv_usec - start_tb.tv_usec));

	if (MEMCACHED_SUCCESS != error) {
		fprintf(stderr, "memcached_set (sha): %s\n", memcached_strerror(memc, error));
		ret = -1;
	}

out:
	memcached_free(memc);
	return 0;
}

