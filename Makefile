CC=gcc

all: dth_client

utils.o: utils.c
	$(CC) -c $<

dth_ops.o: dth_ops.c
	$(CC) -c $<

dth_client.o: dth_client.c
	$(CC) -c $<

dth_client: utils.o dth_ops.o dth_client.o
	$(CC) -o $@ $^ -lcrypto -lmemcached

clean:
	rm -f dth_client *.o
