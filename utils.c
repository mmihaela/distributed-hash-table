#include <stdlib.h>
#include <string.h>

#include "utils.h"

server_info_t servers_md5[NO_IPS];
server_info_t servers_sha[NO_IPS];

void print_hash(unsigned char *digest, int length)
{
	int i;

	for (i = 0; i < length; i++) {
		printf("%02x", digest[i]);
	}

	printf("\n");
}

/**
 * Read input file
 */
int read_operation(FILE *fd, op_t *op_pair)
{
	int ret;
	char op_name[OP_SIZE];

	ret = fscanf(fd, "%s", op_name);
	if (strncmp(op_name, GET, strlen(op_name)) == 0) {
		ret = fscanf(fd, "%s", op_pair->key);
		if (1 != ret)
			return -1;
		ret = GET_TYPE;
		goto out;
	}

	if (strncmp(op_name, PUT, strlen(op_name)) == 0) {
		ret = fscanf(fd, "%s %s", op_pair->key, op_pair->value);
		if (2 != ret)
			return -1;
		ret = PUT_TYPE;
	}

out:
	return ret;
}


/**
 * Comparison function between two hashes, used by the comparison function
 * for qsort.
 */
int compare_hash(unsigned char *hash1, unsigned char *hash2, int size)
{

	int i;

	for (i = 0; i < size; i++) {
		if (hash1[i] < hash2[i])
			return -1;
		else if (hash1[i] > hash2[i])
			return 1;
	}

	return 0;
}

/**
 * Wrapper over the function that compares two hashes. The function is
 * received as parameter by the qsort function, used to sort the servers
 * in ascending order after the hash of thei IP addresses.
 */
static int compare_hash_servers(const void *s1, const void *s2)
{
	server_info_t *server1 = (server_info_t *)s1;
	server_info_t *server2 = (server_info_t *)s2;

	return compare_hash((*server1).hash, (*server2).hash, server1->hash_size);
}

void init_hash_ips()
{
	int i;

	for (i = 1; i <= NO_IPS; i++) {
		sprintf(servers_md5[i - 1].ip, "10.0.0.%d", i);
		memcpy(servers_sha[i - 1].ip, servers_md5[i - 1].ip, strlen(servers_md5[i - 1].ip));

		MD5((unsigned char *)servers_md5[i - 1].ip, strlen(servers_md5[i - 1].ip), (unsigned char *)servers_md5[i - 1].hash);
		SHA((unsigned char *)servers_sha[i - 1].ip, strlen(servers_sha[i - 1].ip), (unsigned char *)servers_sha[i - 1].hash);

		servers_md5[i - 1].hash_size = MD5_DIGEST_LENGTH;
		servers_sha[i - 1].hash_size = SHA_DIGEST_LENGTH;
	}

	/* sort IPs after MD5 hash */
	qsort (servers_md5, NO_IPS, sizeof (server_info_t), compare_hash_servers);

	/* sort IPs after SHA hash */
	qsort (servers_sha, NO_IPS, sizeof (server_info_t), compare_hash_servers);

	#if defined(DEBUG)
	/* print md5 ordered servers */
	printf("MD5 ordered servers:\n");
	for (i = 0; i < NO_IPS; i++) {
		printf("[%i]: ", i);
		print_hash(servers_md5[i].hash, MD5_DIGEST_LENGTH);
	}

	/* print SHA ordered servers */
	printf("SHA ordered servers:\n");
	for (i = 0; i < NO_IPS; i++) {
		printf("[%i]: ", i);
		print_hash(servers_sha[i].hash, SHA_DIGEST_LENGTH);
	}
	#endif
}
